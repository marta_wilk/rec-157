<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contacts".
 *
 * @property integer $id
 * @property string $f_name
 * @property string $l_name
 * @property integer $phone
 * @property string $email
 * @property string $address
 * @property string $city
 * @property string $zip
 * @property integer $is_friend
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['f_name', 'l_name', 'phone', 'email', 'address', 'city', 'zip', 'is_friend'], 'required'],
            [['is_friend'], 'integer'],
            [['address'], 'string'],
            [['f_name', 'l_name', 'city'], 'string', 'max' => 64],
            [['email'], 'string', 'max' => 128],
            [['zip'], 'string', 'max' => 16]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'f_name' => 'First Name',
            'l_name' => 'Last Name',
            'phone' => 'Phone',
            'email' => 'Email',
            'address' => 'Address',
            'city' => 'City',
            'zip' => 'Zip',
            'is_friend' => 'Is Friend?',
        ];
    }
}
