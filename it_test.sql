-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 31 Sie 2015, 16:44
-- Wersja serwera: 5.6.24
-- Wersja PHP: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `it_test`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL,
  `f_name` varchar(64) COLLATE cp1250_polish_ci NOT NULL,
  `l_name` varchar(64) COLLATE cp1250_polish_ci NOT NULL,
  `phone` int(64) NOT NULL,
  `email` varchar(128) COLLATE cp1250_polish_ci NOT NULL,
  `address` text COLLATE cp1250_polish_ci NOT NULL,
  `city` varchar(64) COLLATE cp1250_polish_ci NOT NULL,
  `zip` varchar(6) COLLATE cp1250_polish_ci NOT NULL,
  `is_friend` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=154 DEFAULT CHARSET=cp1250 COLLATE=cp1250_polish_ci;

--
-- Zrzut danych tabeli `contacts`
--

INSERT INTO `contacts` (`id`, `f_name`, `l_name`, `phone`, `email`, `address`, `city`, `zip`, `is_friend`) VALUES
(138, 'Marek', 'Kucharski', 48, 'xKozlowska@Mazur.pl', 'Studzienna 02/81, 18-171 Bydgoszcz', 'Władysławowo', '22-935', 0),
(139, 'Alex', 'Woźniak', 48, 'Blazej53@Baranowski.pl', 'Żniwna 27A, 88-394 Franciszków', 'Pszczyna', '12-693', 1),
(140, 'Artur', 'Wojciechowska', 48, 'yKwiatkowska@Wysocki.info', 'Truskawkowa 61, 31-352 Nowy Dwór Mazowiecki', 'Ełk', '24-995', 0),
(144, 'Szymon', 'Sikorska', 706475080, 'Sokolowska.Nikola@wp.pl', 'Dworcowa 59, 54-137 Nysa', 'Puławy', '86-638', 0),
(145, 'Monika', 'Szewczyk', 48, 'Kacper99@onet.pl', 'Głogowska 96A, 15-564 Wola Kiedrzyńska', 'Łomża', '35-411', 1),
(146, 'Ignacy', 'Błaszczyk', 48, 'Maria.Kalinowski@Szymczak.com.pl', 'Mazowiecka 84A, 64-459 Rynarzewo', 'Bełchatów', '08-924', 0),
(147, 'Kornel', 'Cieślak', 0, 'Norbert.Jakubowski@Nowakowska.pl', 'Majowa 38, 29-456 Łódź', 'Kolonowskie', '69-267', 0),
(148, 'Piotr', 'Marciniak', 48, 'Monika.Mroz@interia.pl', 'Witosa Wincentego 88/38, 58-358 Gdańsk', 'Gołubie', '94-898', 0),
(149, 'Anna', 'Krupa', 0, 'Dagmara52@gazeta.pl', 'Litewska 60A, 35-338 Świnoujście', 'Ostróda', '19-602', 0),
(150, 'Weronika', 'Cieślak', 48, 'Ida02@Piotrowski.org', 'Rubinowa 56/84, 37-585 Czerwionka-Leszczyny', 'Jaworzno', '43-817', 0),
(151, 'Aleksandra', 'Woźniak', 0, 'Jeremi06@yahoo.com', 'Leśmiana Bolesława 66A, 45-945 Jasło', 'Bieruń', '84-922', 0),
(152, 'Oliwia', 'Laskowska', 679691323, 'Paulina31@Jakubowska.com.pl', 'Korczaka Janusza 61/68, 31-514 Kłodzko', 'Tychy', '41-414', 0),
(153, 'Karina', 'Nowak', 48, 'Agnieszka.Czerwinska@Walczak.pl', 'Kasprowicza Jana 66, 56-627 Zielona Góra', 'Jadowniki', '48-376', 0);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=154;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
