<?php

use app\models\Contact;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Contacts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    Modal::begin([
        'header' => '<h2>Create new contact</h2>',
        'toggleButton' => ['label' => 'Create contact','class'=>'btn btn-success'],
    ]);
    $model = new Contact();
    echo $this->render('_form',[ 'model'=>$model]);

    Modal::end();
    ?>

    <?php
    //var_dump( \yii\helpers\Url::current(['delete'=>true]) );
    ?>



<a href="<?= \yii\helpers\Url::current(['delete'=>true]) ?>" class="btn btn-danger"> Delete visible rows </a>

<p style="margin-top:20px">Total number of friends: <?= $friends ?></p>
<p style="margin-top:20px">Total number of contacts: <?= Contact::find()->count() ?></p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'f_name',
            'l_name',
            'phone',
            'email:email',
             'address:ntext',
             'city',
             'zip',
           'is_friend',

            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{delete}{edit}',
                'buttons'=>[
                    'edit' => function ($url, $model) {
                       $modal = Modal::begin([
                            'header' => '<h2>Edit contact</h2>',
                          //  'toggleButton' => ['label' => 'Create contact','class'=>'btn btn-success'],
                        ]);

                        echo $this->render('_form',[ 'model'=>$model]);

                        Modal::end();

                        return Html::a('<span type="button" data-toggle="modal" data-target="#'.$modal->id.'"  class="glyphicon glyphicon-edit"></span>', '#', [
                            'title' => Yii::t('yii', 'Edit'),
                        ]);

                    }
                ]
            ],
        ],
    ]); ?>

</div>
